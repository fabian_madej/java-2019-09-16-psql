CREATE TABLE dragons (
    dragonsId SERIAL PRIMARY KEY,
    name VARCHAR(256),
    color VARCHAR(256),
    wings INT
);

CREATE TABLE eggs (
    eggId SERIAL PRIMARY KEY,
    weight INT,
    diameter INT,
    parent INT,
    FOREIGN KEY (parent) REFERENCES dragons(dragonsId)
);

CREATE TABLE ornaments (
    color VARCHAR(256),
    pattern VARCHAR(256),
    egg INT UNIQUE NOT NULL,
    FOREIGN KEY (egg) REFERENCES eggs(eggId)
);

CREATE TABLE lands (
    PRIMARY KEY (name),
    name VARCHAR(256)
);

CREATE TABLE landsAndDragons(
    dragon INT,
    land VARCHAR(256),
    FOREIGN KEY (dragon) REFERENCES dragons (dragonsId),
    FOREIGN KEY (land) REFERENCES lands (name),
    PRIMARY KEY (dragon,land)
);

INSERT INTO dragons (name, color, wings) VALUES
    ('Dygir','green',200),
    ('Bondril','red',100),
    ('Onosse','black',250),
    ('Chiri','yellow',50),
    ('Lase','blue',300)
;

INSERT INTO eggs (weight, diameter, parent) VALUES
    (300, 20, 1),
    (340, 30, 1),
    (200, 10, 2),
    (230, 20, 3),
    (300, 25, 3),
    (200, 15, 3)
;

INSERT INTO ornaments (color, pattern, egg) VALUES
    ('pink','mesh',1),
    ('black','striped',2),
    ('yellow','dotted',3),
    ('blue','dotted',4),
    ('green','striped',5),
    ('red','mesh',6)
;

INSERT INTO lands (name) VALUES
    ('Froze'),
    ('Oswia'),
    ('Oscyea'),
    ('Oclurg')
;

INSERT INTO landsAndDragons (dragon, land) VALUES
    (1,'Froze'),
    (2,'Froze'),
    (1,'Oswia'),
    (3,'Oswia'),
    (4,'Oswia')
;

CREATE VIEW eggsAndOrnaments AS
    SELECT eggs.diameter, eggs.weight,
    ornaments.color,ornaments.pattern 
    FROM ornaments 
    RIGHT JOIN eggs ON ornaments.egg = eggs.eggId
;

CREATE VIEW dragonsAndLands AS
    SELECT d.name AS Dragon, l.name AS Land 
    FROM landsAndDragons AS ld
    INNER JOIN dragons AS d ON ld.dragon = d.dragonsId
    INNER JOIN lands AS l ON ld.land = l.name
;

CREATE VIEW dragonsWithEggs AS
    SELECT d.name, e.weight, e.diameter
    FROM eggs AS e
    INNER JOIN dragons AS d ON e.parent = d.dragonsId
;

CREATE VIEW dragonsWithoutEggs AS
    SELECT d.name
    FROM eggs AS e
    RIGHT JOIN dragons AS d ON e.parent = d.dragonsId
    WHERE e.parent IS NULL
;

CREATE VIEW from2To4WingsSize AS
    SELECT name FROM dragons
    WHERE wings >= 200 AND wings <= 400
;

SELECT * FROM dragons;

SELECT * FROM eggs;

SELECT * FROM ornaments;

SELECT * FROM lands;

SELECT * FROM landsAndDragons;

SELECT * FROM eggsAndOrnaments;

SELECT * FROM dragonsAndLands;

SELECT * FROM dragonsWithEggs;

SELECT * FROM dragonsWithoutEggs;

SELECT * FROM from2To4WingsSize;


DROP VIEW eggsAndOrnaments;

DROP VIEW dragonsAndLands;

DROP VIEW dragonsWithEggs;

DROP VIEW dragonsWithoutEggs;

DROP VIEW from2To4WingsSize;

DROP TABLE landsAndDragons;

DROP TABLE lands;

DROP TABLE ornaments;

DROP TABLE eggs;

DROP TABLE dragons;


